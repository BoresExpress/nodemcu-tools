{
	"name": "nodemcu-tools",
	"publisher": "boressoft",
	"displayName": "NodeMCU-Tools",
	"description": "NodeMCU development tools for Visual Studio Code",
	"version": "3.5.0",
	"author": {
		"name": "Boris Gulay",
		"email": "boris@gulay.name"
	},
	"license": "MIT",
	"repository": {
		"type": "git",
		"url": "https://BoresExpress@bitbucket.org/BoresExpress/nodemcu-tools.git"
	},
	"bugs": {
		"url": "https://bitbucket.org/BoresExpress/nodemcu-tools/issues"
	},
	"engines": {
		"vscode": "^1.53.0"
	},
	"categories": [
		"Programming Languages",
		"Other"
	],
	"keywords": [
		"nodemcu",
		"iot",
		"lua",
		"nodemcu"
	],
	"activationEvents": [
		"workspaceContains:.nodemcutool",
		"onView:nodemcu-tools.devices"
	],
	"main": "./out/extension.js",
	"contributes": {
		"commands": [
			{
				"command": "nodemcu-tools.connect",
				"title": "Connect",
				"category": "NodeMCU"
			},
			{
				"command": "nodemcu-tools.disconnect",
				"title": "Disonnect",
				"category": "NodeMCU"
			},
			{
				"command": "nodemcu-tools.refreshTreeView",
				"title": "Refresh",
				"category": "NodeMCU",
				"icon": {
					"light": "resources/light/refresh.svg",
					"dark": "resources/dark/refresh.svg"
				}
			},
			{
				"command": "nodemcu-tools.deleteFile",
				"title": "Delete",
				"category": "NodeMCU"
			},
			{
				"command": "nodemcu-tools.compileFile",
				"title": "Compile",
				"category": "NodeMCU",
				"enablement": "viewItem == nodemcu-file-lua"
			},
			{
				"command": "nodemcu-tools.runFile",
				"title": "Run",
				"category": "NodeMCU",
				"enablement": "viewItem =~ /^nodemcu-file-(lua|lc)$/"
			},
			{
				"command": "nodemcu-tools.uploadFile",
				"title": "Upload to device",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder"
			},
			{
				"command": "nodemcu-tools.uploadFileAs",
				"title": "Upload to device...",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder"
			},
			{
				"command": "nodemcu-tools.uploadFileCompile",
				"title": "Upload to device and compile",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder && resourceExtname == .lua"
			},
			{
				"command": "nodemcu-tools.downloadFile",
				"title": "Download",
				"category": "NodeMCU"
			},
			{
				"command": "nodemcu-tools.downloadFileAs",
				"title": "Download...",
				"category": "NodeMCU"
			},
			{
				"command": "nodemcu-tools.uploadFileRun",
				"title": "Run on device",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder && resourceExtname == .lua"
			},
			{
				"command": "nodemcu-tools.uploadFileSetLfs",
				"title": "Upload to device and set as LFS",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder && resourceExtname == .img"
			},
			{
				"command": "nodemcu-tools.uploadFileSetLfsAs",
				"title": "Upload to device and set as LFS...",
				"category": "NodeMCU",
				"enablement": "explorerViewletFocus && !explorerResourceIsFolder && resourceExtname == .img"
			}
		],
		"menus": {
			"explorer/context": [
				{
					"command": "nodemcu-tools.uploadFile",
					"alt": "nodemcu-tools.uploadFileAs",
					"group": "nodemcu-tools@10",
					"when": "nodemcu-tools:isConnected"
				},
				{
					"command": "nodemcu-tools.uploadFileSetLfs",
					"alt": "nodemcu-tools.uploadFileSetLfsAs",
					"group": "nodemcu-tools@20",
					"when": "nodemcu-tools:isConnected"
				},
				{
					"command": "nodemcu-tools.uploadFileCompile",
					"alt": "nodemcu-tools.uploadFileRun",
					"group": "nodemcu-tools@20",
					"when": "nodemcu-tools:isConnected"
				}
			],
			"view/title": [
				{
					"command": "nodemcu-tools.refreshTreeView",
					"when": "view == nodemcu-tools.devices",
					"group": "navigation"
				}
			],
			"view/item/context": [
				{
					"command": "nodemcu-tools.connect",
					"when": "view == nodemcu-tools.devices && viewItem == nodemcu-device",
					"group": "nodemcu-tools"
				},
				{
					"command": "nodemcu-tools.disconnect",
					"when": "view == nodemcu-tools.devices && viewItem == nodemcu-device-connected",
					"group": "nodemcu-tools"
				},
				{
					"command": "nodemcu-tools.downloadFile",
					"alt": "nodemcu-tools.downloadFileAs",
					"when": "view == nodemcu-tools.devices && viewItem =~ /^nodemcu-file/",
					"group": "nodemcu-tools@10"
				},
				{
					"command": "nodemcu-tools.runFile",
					"when": "view == nodemcu-tools.devices && viewItem =~ /^nodemcu-file/",
					"group": "nodemcu-tools@20"
				},
				{
					"command": "nodemcu-tools.compileFile",
					"when": "view == nodemcu-tools.devices && viewItem =~ /^nodemcu-file/",
					"group": "nodemcu-tools@30"
				},
				{
					"command": "nodemcu-tools.deleteFile",
					"when": "view == nodemcu-tools.devices && viewItem =~ /^nodemcu-file/",
					"group": "nodemcu-tools@40"
				}
			]
		},
		"views": {
			"explorer": [
				{
					"id": "nodemcu-tools.devices",
					"name": "NodeMCU Devices"
				}
			]
		},
		"configuration": {
			"title": "NodeMCU-Tools",
			"properties": {
				"nodemcu-tools.terminal.scrollbackSize": {
					"type": "number",
					"minimum": 10,
					"maximum": 1000,
					"default": 300,
					"description": "How many lines will be held in terminal. Old lines over this limit will be discarded."
				},
				"nodemcu-tools.terminal.commandHistorySize": {
					"type": "number",
					"minimum": 0,
					"maximum": 100,
					"default": 30,
					"description": "How many commands will be available by pressing UP in command input field. Old commands over this limit will be discarded."
				},
				"nodemcu-tools.snippets": {
					"type": "object",
					"default": {
						"Restart": "node.restart()",
						"Heap": "=node.heap()",
						"ChipID": "=node.chipid()",
						"WiFi Status": "local wifista=wifi.sta.status();for k,v in pairs(wifi) do if v==wifista and string.sub(k,1,4)=='STA_' then print(k) end end",
						"WiFi IP": "=wifi.sta.getip()",
						"WiFi APs": "wifi.sta.getap({show_hidden=1},1,function(t) for k,v in pairs(t) do print(k..':'..v) end end)"
					},
					"description": "Command snippets"
				},
				"nodemcu-tools.completion.enabled": {
					"type": "boolean",
					"default": true,
					"description": "Is language completion provider for LUA provided by extension active or not. Switch to false if you have another language server for LUA installed and active."
				}
			}
		}
	},
	"scripts": {
		"vscode:prepublish": "webpack --mode production",
		"build": "webpack --mode development",
		"lint": "eslint src"
	},
	"devDependencies": {
		"@types/bindings": "^1.5.1",
		"@types/node": "^16.9.0",
		"@types/react": "^18.0.15",
		"@types/react-dom": "^18.0.6",
		"@types/serialport": "^8.0.2",
		"@types/styled-components": "^5.1.25",
		"@types/vscode": "^1.53.0",
		"@types/webpack": "^5.28.0",
		"@typescript-eslint/eslint-plugin": "^5.31.0",
		"@typescript-eslint/parser": "^5.31.0",
		"eslint": "^8.20.0",
		"eslint-plugin-optimize-regex": "^1.2.1",
		"eslint-plugin-promise": "^6.0.0",
		"eslint-plugin-react": "^7.30.1",
		"eslint-plugin-react-hooks": "^4.6.0",
		"eslint-plugin-sonarjs": "^0.14.0",
		"eslint-webpack-plugin": "^3.2.0",
		"fork-ts-checker-webpack-plugin": "^7.2.13",
		"ts-loader": "^9.3.1",
		"ts-node": "^10.9.1",
		"typescript": "^4.7.4",
		"webpack": "^5.74.0",
		"webpack-cli": "^4.10.0"
	},
	"dependencies": {
		"effector": "^22.3.0",
		"effector-react": "^22.1.5",
		"react": "^18.2.0",
		"react-dom": "^18.2.0",
		"react-tooltip": "^4.2.21",
		"serialport": "^10.4.0",
		"styled-components": "^5.3.5"
	}
}
